package main.java.com.lists.services.implementations;

import main.java.com.lists.services.interfaces.IList2;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

public class AList2<T> implements IList2<T> {
    private int capacity;
    public Object[] array;
    private int size;

    public AList2() {
        this.capacity = 10;
        array = new Object[this.capacity];
        size = 0;
    }

    public AList2(int capacity) {
        this.capacity = capacity;
        array = new Object[this.capacity];
        size = 0;
    }

    public AList2(T[] array) {
        this.array = array;
        this.capacity = this.array.length;
        size = array.length;
    }

    @Override
    public void clear() {
        this.capacity = 10;
        array = new Object[this.capacity];
        size = 0;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public T get(int index) {
        if (index >= size || index < 0) {
            return null;
        }
        else {
            T value = (T) array[index];
            return value;
        }
    }

    @Override
    public boolean add(T value) {
        return add(size, value);
    }

    @Override
    public boolean add(int index, T value) {
        if (value == null) {
            return false;
        }
        else if (index > size || index < 0) {
            return false;
        }
        else {
            if (size == capacity) {
                increaseCapacity();
            }

            Object[] newArray = new Object[capacity];
            int oldIndex = 0;
            for (int i = 0; i < newArray.length; i++) {
                if (index == i) {
                   newArray[i] = value;
                }
                else {
                    newArray[i] = array[oldIndex];
                    oldIndex++;
                }
            }

            array = newArray;
            size++;
            return true;
        }
    }

    @Override
    public T remove(T element) {
        T removedElement = null;
        for (int i = 0; i < size; i++) {
            if (array[i].equals(element)) {
                removedElement = removeByIndex(i);
                break;
            }
        }
        return removedElement;
    }

    @Override
    public T removeByIndex(int index) {
        if (index < 0 || index >= size) {
            return null;
        }
        else {
            T removedElement = (T) array[index];
            Object[] newArray = new Object[capacity];
            int newIndex = 0;
            for (int i = 0; i < array.length; i++) {
                if (i != index){
                    newArray[newIndex] = array[i];
                    newIndex++;
                }
            }
            array = newArray;
            size--;
            return  removedElement;
        }
    }

    @Override
    public boolean contains(T value) {
        for (int i = 0; i < size; i++) {
            if (array[i].equals(value)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean set(int index, Object value) {
        if (value == null) {
            return false;
        }
        else if (index >= size || index < 0) {
            return false;
        }
        else {
            array[index] = value;
            return true;
        }
    }

    @Override
    public void print() {
        String stringArray = "[";
        for (int i = 0; i < size; i++) {
            stringArray += array[i];
            if (i != size - 1) {
                stringArray += ", ";
            }
        }
        stringArray += "]\n";
        System.out.println(stringArray);
    }

    @Override
    public Object[] toArray() {
        return Arrays.copyOf(array, size);
    }

    @Override
    public boolean removeAll(T[] arr) {
        if (arr == null){
            return false;
        }
        for (int i = 0; i < arr.length; i++) {
            remove(arr[i]);
        }
        return true;
    }

    public static void main(String[] args) {
        ArrayList<Integer> arrayList = new ArrayList<>();
//        arrayList.add(3);
//        arrayList.add(4);
//        arrayList.add(5);
//        Object[] integers = arrayList.toArray();
//        for (int i = 0; i < integers.length; i++) {
//            System.out.println(integers[i]);
//        }

        AList2<Integer> aList2 = new AList2<>();
        aList2.add(3);
        aList2.add(3);
        aList2.add(4);
        aList2.add(6);
        aList2.add(7);
        aList2.add(8);
        aList2.add(9);
        Integer[] integers = {3, 6, 5, 2, 8};
        aList2.removeAll(integers);
        aList2.print();
    }

    private void increaseCapacity(){
        capacity = capacity * 3 / 2;
        Object[] newArray = new Object[capacity];
        for (int i = 0; i < array.length; i++) {
            newArray[i] = array[i];
        }
        array = newArray;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AList2<?> aList2 = (AList2<?>) o;
        return Arrays.equals(array, aList2.array);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(array);
    }
}
