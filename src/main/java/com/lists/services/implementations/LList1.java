package main.java.com.lists.services.implementations;

import main.java.com.lists.services.interfaces.ILinkedList;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;

public class LList1<T> implements ILinkedList<T> {
    private int size = 0;
    private Node<T> first;
    private Node<T> last;

    public LList1(){

    }

    public LList1(Collection<T> collection){
        Iterator<T> iterator = collection.iterator();
        while (iterator.hasNext()) {
            T element = iterator.next();
            add(element);
            size++;
        }
    }

    @Override
    public void clear() {
        first = null;
        last = null;
        size = 0;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public T get(int index) {
        if (index < 0 || index >= size) {
            return null;
        }
        else if (index == size - 1) {
            return last.item;
        }
        else if (index == 0) {
            return first.item;
        }
        else {
            T itemToGet = null;
            Node<T> nodeToGet = first;
            for (int i = 1; i < size - 1; i++) {
                nodeToGet = nodeToGet.next;
                if (i == index) {
                    itemToGet = nodeToGet.item;
                }
            }
            return itemToGet;
        }
    }

    @Override
    public boolean add(T value) {
        if (value == null) {
            return false;
        }
        Node<T> nodeToAdd = new Node<>(value, null);
        if (last == null) {
            first = nodeToAdd;
        }
        else {
            last.next = nodeToAdd;
        }
        last = nodeToAdd;
        size++;
        return true;
    }

    @Override
    public boolean addFirst(T value) {
        if (value == null) {
            return false;
        }
        Node<T> nodeToAdd = new Node<>(value, first);
        if (last == null) {
            last = nodeToAdd;
        }
        first = nodeToAdd;
        size++;
        return true;
    }

    @Override
    public boolean addLast(T value) {
        return add(value);
    }

    @Override
    public boolean add(int index, T value) {
        if (value == null) {
            return false;
        }
        else if (index < 0 || index > size) {
            return false;
        }
        else {
            if (index == size) {
                return addLast(value);
            }
            else if (index == 0) {
                return addFirst(value);
            }
            else {
                Node<T> prevNode = first;
                Node<T> nextNode = null;
                for (int i = 1; i < size; i++) {
                    nextNode = prevNode.next;
                    if (i == index) {
                        Node<T> newNode = new Node<>(value, nextNode);
                        prevNode.next = newNode;
                        break;
                    }
                    prevNode = nextNode;
                }
                size++;
                return true;
            }
        }
    }

    @Override
    public T remove(T element) {
        if (element == null) {
            return null;
        }
        else if (first.item.equals(element)) {
            T valToReturn = first.item;
            first = first.next;
            size--;
            if (size == 1) {
                last = null;
            }
            return valToReturn;
        }
        else {
            T valToReturn = null;
            Node<T> prevNode = first;
            Node<T> nextNode = null;
            for (int i = 1; i < size; i++) {
                nextNode = prevNode.next;
                if (nextNode.item.equals(element)) {
                    valToReturn = nextNode.item;
                    prevNode.next = nextNode.next;
                    if (i == size - 1) {
                        last = prevNode;
                    }
                    break;
                }
                prevNode = nextNode;
            }
            size--;
            return valToReturn;
        }
    }

    @Override
    public T removeFirst() {
        return removeByIndex(0);
    }

    @Override
    public T removeLast() {
        return removeByIndex(size - 1);
    }

    @Override
    public T removeByIndex(int index) {
        if (index < 0 || index >= size) {
            return null;
        }
        else if (size == 1) {
            T valToReturn = last.item;
            first = null;
            last = null;
            size--;
            return valToReturn;
        }
        else if (index == 0) {
            T valToReturn = first.item;
            first = first.next;
            size--;
            return valToReturn;
        }
        else {
            T valToReturn = null;
            Node<T> prevNode = first;
            Node<T> nextNode = null;
            for (int i = 1; i < size; i++) {
                nextNode = prevNode.next;
                if (i == index) {
                    valToReturn = nextNode.item;
                    prevNode.next = nextNode.next;
                    if (i == size - 1) {
                        last = prevNode;
                    }
                    break;
                }
                prevNode = nextNode;
            }
            size--;
            return valToReturn;
        }
    }

    @Override
    public boolean contains(T value) {
        if (value == null) {
            return false;
        }
        else if (last.item.equals(value)) {
            return true;
        }
        else if (first.item.equals(value)) {
            return true;
        }
        else{
            boolean doesContain = false;
            Node<T> currentNode = first;
            for (int i = 1; i < size - 1; i++) {
                currentNode = currentNode.next;
                if (currentNode.item.equals(value)) {
                    doesContain = true;
                    break;
                }
            }
            return doesContain;
        }
    }

    @Override
    public boolean set(int index, T value) {
        if (value == null) {
            return false;
        }
        else if (index < 0 || index >= size) {
            return false;
        }
        else if (index == size - 1) {
            last.item = value;
            return true;
        }
        else if (index == 0) {
            first.item = value;
            return true;
        }
        else {
            Node<T> currentNode = first;
            for (int i = 1; i < size; i++) {
                currentNode = currentNode.next;
                if (i == index) {
                    currentNode.item = value;
                    break;
                }
            }
            return true;
        }
    }

    @Override
    public void print() {
        String stringArray = "[";
        if (size != 0) {
            T valToAdd = null;
            Node<T> prevNode = first;
            valToAdd = first.item;
            stringArray += valToAdd;
            Node<T> nextNode = null;
            for (int i = 1; i < size; i++) {
                stringArray += ", ";
                nextNode = prevNode.next;
                stringArray += nextNode.item;
                prevNode = nextNode;
            }
        }
        stringArray += "]\n";
        System.out.println(stringArray);
    }

    @Override
    public Object[] toArray() {
        Object[] listArray = new Object[size];
        if (size != 0) {
            Node<T> currentNode = first;
            listArray[0] = currentNode.item;
            for (int i = 1; i < size; i++) {
                currentNode = currentNode.next;
                listArray[i] = currentNode.item;
            }
        }
        return listArray;
    }

    @Override
    public boolean removeAll(T[] arr) {
        if (arr == null) {
            return false;
        }
        else {
            for (int i = 0; i < arr.length; i++) {
                remove(arr[i]);
            }
            return true;
        }
    }

    public static void main(String[] args) {
        LinkedList<Integer> listTest = new LinkedList<>();
        listTest.add(3);
        listTest.add(34);
        listTest.add(12);

        LList1<Integer> lList1 = new LList1<>();
        lList1.add(3);
        lList1.add(4);
        lList1.add(5);
        lList1.add(6);
        lList1.add(7);

        Integer[] valsToRemove = new Integer[] {7, 3, 5};
        lList1.removeAll(valsToRemove);
//        lList1.removeByIndex(4);
//        lList1.removeByIndex(0);
//        lList1.removeByIndex(2);
//        lList1.removeByIndex(22);
//
        for (int i = 0; i < lList1.size(); i++) {
            System.out.println(lList1.get(i));
        }

//        System.out.println(lList1.contains(44));
//        System.out.println(lList1.contains(7));
//        System.out.println(lList1.contains(3));
//        System.out.println(lList1.contains(5));

//        Object[] lListArray = lList1.toArray();
//        for (int i = 0; i < lListArray.length; i++) {
//            System.out.println(lListArray[i]);
//        }
    }

    private static class Node<T>{
        T item;
        Node<T> next;

        public Node(T item, Node<T> next){
            this.item = item;
            this.next = next;
        }
    }


}
