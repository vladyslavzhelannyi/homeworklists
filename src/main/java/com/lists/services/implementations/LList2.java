package main.java.com.lists.services.implementations;

import main.java.com.lists.services.interfaces.ILinkedList;

import java.util.Collection;
import java.util.Iterator;

public class LList2<T> implements ILinkedList<T> {
    private int size = 0;
    private LList2.Node<T> first;
    private LList2.Node<T> last;

    public LList2(){

    }

    public LList2(Collection<T> collection){
        Iterator<T> iterator = collection.iterator();
        while (iterator.hasNext()) {
            T element = iterator.next();
            add(element);
            size++;
        }
    }

    @Override
    public void clear() {
        first = null;
        last = null;
        size = 0;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public T get(int index) {
        if (index < 0 || index >= size) {
            return null;
        }
        else if (index == size - 1) {
            return last.item;
        }
        else if (index == 0) {
            return first.item;
        }
        else {
            T itemToGet = null;
            Node<T> nodeToGet = first;
            for (int i = 1; i < size - 1; i++) {
                nodeToGet = nodeToGet.next;
                if (i == index) {
                    itemToGet = nodeToGet.item;
                }
            }
            return itemToGet;
        }
    }

    @Override
    public boolean add(T value) {
        return addLast(value);
    }

    @Override
    public boolean addFirst(T value) {
        if (value == null) {
            return false;
        }
        else if (size == 0) {
            Node<T> nodeToAdd = new Node<> (value, null, null);
            first = nodeToAdd;
            last = nodeToAdd;
            size++;
            return true;
        }
        else {
            Node<T> nodeToAdd = new Node<> (value, null, first);
            first.prev = nodeToAdd;
            first = nodeToAdd;
            size++;
            return true;
        }
    }

    @Override
    public boolean addLast(T value) {
        if (value == null) {
            return false;
        }
        else if (size == 0) {
            Node<T> nodeToAdd = new Node<> (value, null, null);
            first = nodeToAdd;
            last = nodeToAdd;
            size++;
            return true;
        }
        else {
            Node<T> nodeToAdd = new Node<> (value, last, null);
            last.next = nodeToAdd;
            last = nodeToAdd;
            size++;
            return true;
        }
    }

    @Override
    public boolean add(int index, T value) {
        if (value == null) {
            return false;
        }
        else if (index < 0 || index > size) {
            return false;
        }
        else {
            if (index == 0) {
                return addFirst(value);
            }
            else if (index == size) {
                return addLast(value);
            }
            else {
                Node<T> prevNode = first;
                Node<T> nextNode = null;
                for (int i = 1; i < size; i++) {
                    nextNode = prevNode.next;
                    if (i == index) {
                        Node<T> newNode = new Node<>(value, prevNode, nextNode);
                        prevNode.next = newNode;
                        newNode.prev = newNode;
                        break;
                    }
                    prevNode = nextNode;
                }
                size++;
                return true;
            }
        }
    }

    @Override
    public T remove(T element) {
        if (element == null) {
            return null;
        }
        else if (first.item.equals(element)) {
            return removeFirst();
        }
        else if (last.item.equals(element)) {
            return removeLast();
        }
        else {
            T valToReturn = null;
            Node<T> prevNode = first;
            Node<T> nextNode = null;
            for (int i = 1; i < size; i++) {
                nextNode = prevNode.next;
                if (nextNode.item.equals(element)) {
                    valToReturn = nextNode.item;
                    Node<T> newNextNode = nextNode.next;
                    prevNode.next = newNextNode;
                    newNextNode.prev = prevNode;
                    size--;
                    break;
                }
                prevNode = nextNode;
            }
            return valToReturn;
        }
    }

    @Override
    public T removeFirst() {
        if (size == 0) {
            return null;
        }
        else {
            T valueToRemove = first.item;
            if (size == 1) {
                first = null;
                last = null;
            }
            else {
                Node<T> nextNode = first.next;
                first = nextNode;
                first.prev = null;
            }
            size--;
            return valueToRemove;
        }
    }

    @Override
    public T removeLast() {
        if (size == 0) {
            return null;
        }
        else {
            T valueToRemove = last.item;
            if (size == 1) {
                first = null;
                last = null;
            }
            else {
                Node<T> prevNode = last.prev;
                last = prevNode;
                last.next = null;
            }
            size--;
            return valueToRemove;
        }
    }

    @Override
    public T removeByIndex(int index) {
        if (index < 0 || index >= size) {
            return null;
        }
        else if (index == 0) {
            return removeFirst();
        }
        else if (index == size - 1) {
            return removeLast();
        }
        else {
            T valToReturn = null;
            Node<T> prevNode = first;
            Node<T> nextNode = null;
            for (int i = 1; i < size; i++) {
                nextNode = prevNode.next;
                if (i == index) {
                    valToReturn = nextNode.item;
                    Node<T> newNextNode = nextNode.next;
                    prevNode.next = newNextNode;
                    newNextNode.prev = prevNode;
                    size--;
                    break;
                }
                prevNode = nextNode;
            }
            return valToReturn;
        }
    }

    @Override
    public boolean contains(T value) {
        if (value == null) {
            return false;
        }
        else if (last.item.equals(value)) {
            return true;
        }
        else if (first.item.equals(value)) {
            return true;
        }
        else{
            boolean doesContain = false;
            Node<T> currentNode = first;
            for (int i = 1; i < size - 1; i++) {
                currentNode = currentNode.next;
                if (currentNode.item.equals(value)) {
                    doesContain = true;
                    break;
                }
            }
            return doesContain;
        }
    }

    @Override
    public boolean set(int index, T value) {
        if (value == null) {
            return false;
        }
        else if (index < 0 || index >= size) {
            return false;
        }
        else if (index == size - 1) {
            last.item = value;
            return true;
        }
        else if (index == 0) {
            first.item = value;
            return true;
        }
        else {
            Node<T> currentNode = first;
            for (int i = 1; i < size; i++) {
                currentNode = currentNode.next;
                if (i == index) {
                    currentNode.item = value;
                    break;
                }
            }
            return true;
        }
    }

    @Override
    public void print() {
        String stringArray = "[";
        if (size != 0) {
            T valToAdd = null;
            Node<T> prevNode = first;
            valToAdd = first.item;
            stringArray += valToAdd;
            Node<T> nextNode = null;
            for (int i = 1; i < size; i++) {
                stringArray += ", ";
                nextNode = prevNode.next;
                stringArray += nextNode.item;
                prevNode = nextNode;
            }
        }
        stringArray += "]\n";
        System.out.println(stringArray);
    }

    @Override
    public Object[] toArray() {
        Object[] listArray = new Object[size];
        if (size != 0) {
            Node<T> currentNode = first;
            listArray[0] = currentNode.item;
            for (int i = 1; i < size; i++) {
                currentNode = currentNode.next;
                listArray[i] = currentNode.item;
            }
        }
        return listArray;
    }

    @Override
    public boolean removeAll(T[] arr) {
        if (arr == null) {
            return false;
        }
        else {
            for (int i = 0; i < arr.length; i++) {
                remove(arr[i]);
            }
            return true;
        }
    }

    public static void main(String[] args) {
        LList2<Integer> l = new LList2<>();
        l.print();

//        l.add(3);
//        l.add(null);
//        l.add(5);
//        l.addFirst(7);
//        l.addLast(7);
//        l.add(2, 45);

        l.add(45);
        l.add(54);
        l.add(76);
        l.add(98);
        l.add(78);

//        l.remove(76);
//        l.remove(45);
//        l.remove(78);
//        l.remove(444);
//        l.remove(444);

//        l.removeAll(new Integer[] {3, 4, 45, 78, 54, 0, null});

//        l.removeByIndex(4);
//        l.removeByIndex(0);
//        l.removeByIndex(1);
        l.print();
//        for (int i = 0; i < l.size; i++) {
//            System.out.println(l.get(i));
//        }
    }

    private static class Node<T>{
        T item;
        Node<T> prev;
        Node<T> next;

        public Node(T item, Node<T> prev, Node<T> next){
            this.item = item;
            this.prev = prev;
            this.next = next;
        }
    }
}
