package main.java.com.lists.services.interfaces;

public interface IList2<T> {
    void clear();

    int size();

    T get(int index);

    boolean add(T value);

    boolean add(int index, T value);

    T remove(T element);

    T removeByIndex(int index);

    boolean contains(T value);

    boolean set(int index, T value);

    void print(); //=> выводит в консоль массив в квадратных скобка и через запятую

    Object[] toArray(); //=> приводит данные к массиву, в случае с AList ничего сложного, у нас и так массив

    boolean removeAll(T[] arr);
}
