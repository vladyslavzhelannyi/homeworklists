package main.java.com.lists.services.JustTest;

public class ListTest<T> {
    private Object[] ttt;

    public ListTest(T[] ttt){
        this.ttt = ttt;

    }

    public T[] get(){
        return (T[])ttt;
    }

    public static void main(String[] args) {
        Integer[] integers = new Integer[] {3, 4, 5, null};
        ListTest<Integer> listTest = new ListTest<>(integers);
        Integer[] aaa = listTest.get();
        for (int i = 0; i < aaa.length; i++) {
            System.out.println(aaa[i]);
        }
    }
}
